<?php
/*
Plugin Name: Zabit logo
Description: Deze plugin voegt het Zabit logo toe aan de website.
Author: Zabit
Version: 1.0.2
*/

function zabit_logo_add_scripts() {

    wp_enqueue_style( 
        'zabit-logo-style', 
        plugins_url('css/style.css', __FILE__), 
        array(), 
        '1.0.2', 
        'all' 
    );

    wp_enqueue_script(
        'zabit-logo-js',
        plugins_url('js/script.js', __FILE__),
        array( 'jquery' ), 
        '1.0.2', 
        true 
    );

}
add_action( 'wp_enqueue_scripts', 'zabit_logo_add_scripts' );
